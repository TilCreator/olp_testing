use olp_colors::*;
use sdl2;

fn main() {
    println!(
        "Hello rainbow! {}",
        Srgb::from(Hsv {
            h: 1.0,
            s: 1.0,
            v: 1.0
        })
    )
}
