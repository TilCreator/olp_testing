use glutin_window;
use graphics;
use opengl_graphics;
use piston;
use olp_colors;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
    state: f64,  // Rotation for the square.
}

pub struct DebugParam {
    pub x: f64,
    pub y: f64,
    pub state: f64,
}

impl App {
    fn render(&mut self, args: &RenderArgs, n: i16) {
        use graphics::*;

        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

        let squaes: Vec<types::Rectangle> = (0 .. (args.window_size[0]as i16/n)).map(|i| rectangle::square((n*i) as f64, 0.0, i as f64)).collect();

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(BLACK, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        // Rotate 2 radians per second.
        self.state += 2.0 * args.dt % 1.0
    }

    fn render_color() -> [f64; 4] {

    }
}

fn main() {
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    let window_x = 2048;
    let window_y = 64;
    let pixels = 256;

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new("spinning-square", [window_x, window_y])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
        rotation: 0.0,
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            app.render(&args, pixels);
        }

        if let Some(args) = e.update_args() {
            app.update(&args);
        }
    }
}
