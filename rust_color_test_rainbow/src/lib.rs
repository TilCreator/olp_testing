const GAMMA: f64 = 2.2;

// Initializing a clonable struct for each color space.
#[derive(Clone, Debug)]
pub struct Hsv {
    pub h: f64,
    pub s: f64,
    pub v: f64,
}

#[derive(Clone, Debug)]
pub struct LinRgb {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

#[derive(Clone, Debug)]
pub struct LinRgbw {
    pub r: f64,
    pub g: f64,
    pub b: f64,
    pub w: f64,
}

// Gamma encoded structs of each type being sent to LEDs using i8.
#[derive(Clone, Debug)]
pub struct Srgb {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub gamma: f64,
}

#[derive(Clone, Debug)]
pub struct EncRgbw {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub w: u8,
    pub gamma: f64,
}

// Defining traits each color space must implement.
pub trait OlpColor:
    std::clone::Clone
    + std::fmt::Debug
    + std::fmt::Display
    + From<LinRgb>
    + From<Hsv>
    + From<Srgb>
    + From<LinRgbw>
    + From<EncRgbw>
{
}

impl OlpColor for LinRgb {}
impl PartialEq for LinRgb {
    fn eq(&self, other: &Self) -> bool {
        let precision = 0.05;

        self.r - precision < other.r
            && self.r + precision > other.r
            && self.g - precision < other.g
            && self.g + precision > other.g
            && self.b - precision < other.b
            && self.b + precision > other.b
    }
}
impl From<Hsv> for LinRgb {
    // Calculating conversion from HSV to linear RGB and returning an LinRgb Color-Struct. Called structs remains unchanged.
    fn from(hsv: Hsv) -> Self {
        // Skip Color calculations, if saturation is 0
        if hsv.s == 0f64 {
            return LinRgb {
                r: hsv.v,
                g: hsv.v,
                b: hsv.v,
            };
        }

        let max_col = hsv.s * hsv.v;
        let grey = hsv.v - max_col;
        let rising_ramp = (hsv.h * 6f64) % 1f64;
        let declining_ramp = 1f64 - rising_ramp;

        match ((6f64 * hsv.h) % 6f64) as u8 {
            0 => LinRgb {
                r: hsv.v,
                g: rising_ramp * max_col + grey,
                b: grey,
            },
            1 => LinRgb {
                g: hsv.v,
                r: declining_ramp * max_col + grey,
                b: grey,
            },
            2 => LinRgb {
                g: hsv.v,
                b: rising_ramp * max_col + grey,
                r: grey,
            },
            3 => LinRgb {
                b: hsv.v,
                g: declining_ramp * max_col + grey,
                r: grey,
            },
            4 => LinRgb {
                b: hsv.v,
                r: rising_ramp * max_col + grey,
                g: grey,
            },
            5 => LinRgb {
                r: hsv.v,
                b: declining_ramp * max_col + grey,
                g: grey,
            },
            _ => unreachable!(),
        }
    }
}
impl From<Srgb> for LinRgb {
    // Calculates and returns a linRGB struct by using it's gamma-value to convert each of it's components to a linear f64-based color space.
    fn from(s_rgb: Srgb) -> Self {
        LinRgb {
            r: (s_rgb.r as f64 / 255.0).powf(s_rgb.gamma),
            g: (s_rgb.g as f64 / 255.0).powf(s_rgb.gamma),
            b: (s_rgb.b as f64 / 255.0).powf(s_rgb.gamma),
        }
    }
}
impl From<EncRgbw> for LinRgb {
    fn from(enc: EncRgbw) -> Self {
        LinRgbw::from(enc).into()
    }
}
impl From<LinRgbw> for LinRgb {
    // Generates and returns a linRGB struct. Generated RGB-Components are the sum of the corresponding RGB-component and the W-component of the called LinRgb Struct.
    fn from(lin_rgbw: LinRgbw) -> Self {
        Self {
            r: lin_rgbw.r + lin_rgbw.w,
            g: lin_rgbw.g + lin_rgbw.w,
            b: lin_rgbw.b + lin_rgbw.w,
        }
    }
}

impl OlpColor for LinRgbw {}
impl PartialEq for LinRgbw {
    fn eq(&self, other: &Self) -> bool {
        let precision = 0.05;

        self.r - precision < other.r
            && self.r + precision > other.r
            && self.g - precision < other.g
            && self.g + precision > other.g
            && self.b - precision < other.b
            && self.b + precision > other.b
            && self.w - precision < other.w
            && self.w + precision > other.w
    }
}
impl From<Hsv> for LinRgbw {
    // Calculates and returns a LinRgbw Struct using get_linrgb to get the first RGB components and then calculates W component. See code comments for more info.
    fn from(hsv: Hsv) -> Self {
        // Makes an Hsv Struct using it's hue, a saturation of 1 and it's own value multiplied by it's saturation.
        // This basically makes the RGB components of an RGBW strip soely responsible for displaying colorful light.
        // If saturation is at 0, the RGB diodes will be off and if saturation is at 1, they will be at full brightness (accounting for the given Value.)
        let LinRgb { r, g, b } = Hsv {
            h: hsv.h,
            s: 1.0f64,
            v: hsv.v * hsv.s,
        }
        .into();

        // Generates W component out of it's value multiplied by the inverse saturation.
        LinRgbw {
            w: 1.0 - (hsv.s * hsv.v),
            r,
            g,
            b,
        }
    }
}
impl From<Srgb> for LinRgbw {
    // Calculates and returns a linRGBW struct by using it's gamma-value to convert each of it's components to a linear f64-based color space.
    fn from(s_rgb: Srgb) -> Self {
        LinRgb::from(s_rgb).into()
    }
}
impl From<LinRgb> for LinRgbw {
    // Calculates and returns linRGBW Color Struct. Uses difference between each of it's components and it's minimum component to calculate the Color-components of RGBW. The white-component is set equal to the minimum component. This removes additive white from RGB components and assignes it to W component.
    fn from(lin_rgb: LinRgb) -> LinRgbw {
        let mut min_cmp: f64 = lin_rgb.r;
        if lin_rgb.g < min_cmp {
            min_cmp = lin_rgb.g
        }
        if lin_rgb.b < min_cmp {
            min_cmp = lin_rgb.b
        }

        LinRgbw {
            r: lin_rgb.r - min_cmp,
            g: lin_rgb.g - min_cmp,
            b: lin_rgb.b - min_cmp,
            w: min_cmp,
        }
    }
}
impl From<EncRgbw> for LinRgbw {
    // Calculates and returns a LinRgbw struct by using it's gamma-value to convert each of it's components to a linear f64-based color space.
    fn from(enc: EncRgbw) -> Self {
        LinRgbw {
            r: (enc.r as f64 / 255.0).powf(enc.gamma),
            g: (enc.g as f64 / 255.0).powf(enc.gamma),
            b: (enc.b as f64 / 255.0).powf(enc.gamma),
            w: (enc.w as f64 / 255.0).powf(enc.gamma),
        }
    }
}

impl OlpColor for Hsv {}
impl PartialEq for Hsv {
    fn eq(&self, other: &Self) -> bool {
        let precision = 0.03;

        // Compares HSV with limited precision. Hue and Saturation are not matched, if saturation is near 0.
        ((self.h % 1.0 - precision < other.h%1.0 &&  self.h % 1.0 + precision > other.h % 1.0 && //compares hue
        self.s - precision < other.s  &&  self.s + precision > other.s) || //compares saturation
        (self.s < precision && other.s < precision )) && //true, if saturations are 0
        self.v - precision < other.v  &&  self.v + precision > other.v //compares value
    }
}
impl From<LinRgbw> for Hsv {
    // Returns an Hsv Color Struct, that was generated from a linRGB clone of itself.
    fn from(lin_rgbw: LinRgbw) -> Hsv {
        LinRgb::from(lin_rgbw).into()
    }
}
impl From<Srgb> for Hsv {
    //To get accurate results, the gamma-encoded Srgb Struct makes a linRGB Struct, wich is used to generate the requested Hsv-struct.
    fn from(s_rgb: Srgb) -> Self {
        LinRgb::from(s_rgb).into()
    }
}
impl From<LinRgb> for Hsv {
    // converting linear RGB to HSV
    fn from(lin_rgb: LinRgb) -> Self {
        // initializing empty values for biggest/lowest channel value and index of biggest channel, because rust.
        let max_cmp: f64;
        let min_cmp: f64;
        let max_cmp_index: i8;

        // determining biggest and smallest channel and saving their values
        if lin_rgb.r > lin_rgb.g && lin_rgb.r > lin_rgb.b {
            max_cmp = lin_rgb.r;
            max_cmp_index = 0;
            if lin_rgb.g < lin_rgb.b {
                min_cmp = lin_rgb.g;
            } else {
                min_cmp = lin_rgb.b;
            }
        } else if lin_rgb.g > lin_rgb.b {
            max_cmp = lin_rgb.g;
            max_cmp_index = 1;
            if lin_rgb.r < lin_rgb.b {
                min_cmp = lin_rgb.r;
            } else {
                min_cmp = lin_rgb.b;
            }
        } else {
            max_cmp = lin_rgb.b;
            max_cmp_index = 2;
            if lin_rgb.r < lin_rgb.g {
                min_cmp = lin_rgb.r;
            } else {
                min_cmp = lin_rgb.g;
            }
        }

        // shortcut in case all channels are equal to avoid calculating hue for grey colors.
        if min_cmp == max_cmp {
            return Hsv {
                h: 0.0,
                s: 0.0,
                v: max_cmp,
            };
        }

        // calculating saturation from min-max-delta, calculate relations of channels to pinpoint accurate hue.
        let s: f64 = (max_cmp - min_cmp) / max_cmp;
        let rc: f64 = (max_cmp - lin_rgb.r) / (max_cmp - min_cmp);
        let gc: f64 = (max_cmp - lin_rgb.g) / (max_cmp - min_cmp);
        let bc: f64 = (max_cmp - lin_rgb.b) / (max_cmp - min_cmp);

        let mut h: f64;

        // pinpointing accurate hue
        match max_cmp_index {
            0 => {
                h = bc - gc;
            }
            1 => {
                h = 2.0 + rc - bc;
            }
            _ => {
                h = 4.0 + gc - rc;
            }
        }
        h = (h / 6.0) % 1.0;

        Hsv { h, s, v: max_cmp }
    }
}
impl From<EncRgbw> for Hsv {
    // Calculates and returns a LinRgbw struct by using it's gamma-value to convert each of it's components to a linear f64-based color space.
    fn from(enc_rgbw: EncRgbw) -> Self {
        LinRgbw::from(enc_rgbw).into()
    }
}

impl OlpColor for Srgb {}
impl From<LinRgb> for Srgb {
    // Calculating and returning a gamma encoded Srgb Struct using 8-bit sRGB color space. Clones it's RGB components and makes them "broadcast-legal" before lossy converting from internal linear f64-based color space to gamma-encoded space using i8.
    fn from(lin_rgb: LinRgb) -> Self {
        let LinRgb {
            mut r,
            mut g,
            mut b,
        } = lin_rgb;

        if r < 0.0 {
            r = 0.0;
        }
        if r > 1.0 {
            r = 1.0;
        }
        if g < 0.0 {
            g = 0.0;
        }
        if g > 1.0 {
            g = 1.0;
        }
        if b < 0.0 {
            b = 0.0;
        }
        if b > 1.0 {
            b = 1.0;
        }

        Srgb {
            r: (r.powf(1.0 / GAMMA) * 255.0) as u8,
            g: (g.powf(1.0 / GAMMA) * 255.0) as u8,
            b: (b.powf(1.0 / GAMMA) * 255.0) as u8,
            gamma: GAMMA,
        }
    }
}
impl From<LinRgbw> for Srgb {
    fn from(lin_rgbw: LinRgbw) -> Self {
        LinRgb::from(lin_rgbw).into()
    }
}
impl From<Hsv> for Srgb {
    fn from(hsv: Hsv) -> Self {
        LinRgb::from(hsv).into()
    }
}
impl From<EncRgbw> for Srgb {
    fn from(enc_rgbw: EncRgbw) -> Self {
        LinRgbw::from(enc_rgbw).into()
    }
}

impl OlpColor for EncRgbw {}
impl From<LinRgbw> for EncRgbw {
    // Calculating and returning a gamma encoded EncRgbw Struct using 8-bit encoded RGBW color space. Clones it's RGBW components and makes them "broadcast-legal" before lossy converting from internal linear f64-based color space to gamma-encoded space using i8.
    fn from(lin_rgbw: LinRgbw) -> Self {
        let LinRgbw {
            mut r,
            mut g,
            mut b,
            mut w,
        } = lin_rgbw;

        if r < 0.0 {
            r = 0.0;
        } else if r > 1.0 {
            r = 1.0;
        }
        if g < 0.0 {
            g = 0.0;
        } else if g > 1.0 {
            g = 1.0;
        }
        if b < 0.0 {
            b = 0.0;
        } else if b > 1.0 {
            b = 1.0;
        }
        if w < 0.0 {
            w = 0.0;
        } else if w > 1.0 {
            w = 1.0;
        }

        EncRgbw {
            r: (r.powf(1.0 / GAMMA) * 255.0) as u8,
            g: (g.powf(1.0 / GAMMA) * 255.0) as u8,
            b: (b.powf(1.0 / GAMMA) * 255.0) as u8,
            w: (w.powf(1.0 / GAMMA) * 255.0) as u8,
            gamma: GAMMA,
        }
    }
}
impl From<Hsv> for EncRgbw {
    fn from(hsv: Hsv) -> Self {
        LinRgbw::from(hsv).into()
    }
}
impl From<Srgb> for EncRgbw {
    fn from(s_rgb: Srgb) -> Self {
        LinRgbw::from(s_rgb).into()
    }
}
impl From<LinRgb> for EncRgbw {
    fn from(lin_rgb: LinRgb) -> Self {
        LinRgbw::from(lin_rgb).into()
    }
}

impl std::fmt::Display for LinRgb {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "linear RGB({}, {}, {})", self.r, self.g, self.b)
    }
}
impl std::fmt::Display for LinRgbw {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "linear RGBW({}, {}, {}, {})",
            self.r, self.g, self.b, self.w
        )
    }
}
impl std::fmt::Display for Srgb {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Srgb({}, {}, {}) Gamma: {}",
            self.r, self.g, self.b, self.gamma
        )
    }
}
impl std::fmt::Display for EncRgbw {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "encoded RGBW({}, {}, {}, {}) Gamma: {}",
            self.r, self.g, self.b, self.w, self.gamma
        )
    }
}
impl std::fmt::Display for Hsv {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "HSV({}, {}, {})", self.h, self.s, self.v)
    }
}

// implementing Tests:
#[cfg(test)]
mod tests {
    use crate::*;
    const COL_COUNT: usize = 20;
    #[rustfmt::skip]
    const REFERENCE_COLORS_LINRGB: [LinRgb; COL_COUNT] = [
        /*red*/        LinRgb {r: 1.0  , g: 0.0  , b: 0.0  },
        /*green*/      LinRgb {r: 0.0  , g: 1.0  , b: 0.0  },
        /*blue*/       LinRgb {r: 0.0  , g: 0.0  , b: 1.0  },
        /*cyan*/       LinRgb {r: 0.0  , g: 1.0  , b: 1.0  },
        /*mangenta*/   LinRgb {r: 1.0  , g: 0.0  , b: 1.0  },
        /*yellow*/     LinRgb {r: 1.0  , g: 1.0  , b: 0.0  },
        /*black*/      LinRgb {r: 0.0  , g: 0.0  , b: 0.0  },
        /*white*/      LinRgb {r: 1.0  , g: 1.0  , b: 1.0  },
        /*lightgrey*/  LinRgb {r: 0.7  , g: 0.7  , b: 0.7  },
        /*grey*/       LinRgb {r: 0.5  , g: 0.5  , b: 0.5  },
        /*darkgrey*/   LinRgb {r: 0.3  , g: 0.3  , b: 0.3  },
        /*rose*/       LinRgb {r: 1.0  , g: 0.5  , b: 0.5  },
        /*creme*/      LinRgb {r: 1.0  , g: 1.0  , b: 0.5  },
        /*darkyellow*/ LinRgb {r: 0.5  , g: 0.5  , b: 0.0  },
        /*random1*/    LinRgb {r: 0.42 , g: 0.5  , b: 0.1  },
        /*random2*/    LinRgb {r: 0.58 , g: 0.2  , b: 0.1  },
        /*random3*/    LinRgb {r: 0.97 , g: 0.8  , b: 0.5  },
        /*random4*/    LinRgb {r: 0.0  , g: 0.4  , b: 0.55 },
        /*random5*/    LinRgb {r: 0.1  , g: 0.15 , b: 0.1  },
        /*random6*/    LinRgb {r: 0.4  , g: 0.44 , b: 0.2  },
    ];
    #[rustfmt::skip]
    const REFERENCE_COLORS_LINRGBW: [LinRgbw; COL_COUNT] = [
        /*red*/        LinRgbw {r: 1.0  , g: 0.0  , b: 0.0  , w: 0.0  },
        /*green*/      LinRgbw {r: 0.0  , g: 1.0  , b: 0.0  , w: 0.0  },
        /*blue*/       LinRgbw {r: 0.0  , g: 0.0  , b: 1.0  , w: 0.0  },
        /*cyan*/       LinRgbw {r: 0.0  , g: 1.0  , b: 1.0  , w: 0.0  },
        /*mangenta*/   LinRgbw {r: 1.0  , g: 0.0  , b: 1.0  , w: 0.0  },
        /*yellow*/     LinRgbw {r: 1.0  , g: 1.0  , b: 0.0  , w: 0.0  },
        /*black*/      LinRgbw {r: 0.0  , g: 0.0  , b: 0.0  , w: 0.0  },
        /*white*/      LinRgbw {r: 0.0  , g: 0.0  , b: 0.0  , w: 1.0  },
        /*lightgrey*/  LinRgbw {r: 0.0  , g: 0.0  , b: 0.0  , w: 0.7  },
        /*grey*/       LinRgbw {r: 0.0  , g: 0.0  , b: 0.0  , w: 0.5  },
        /*darkgrey*/   LinRgbw {r: 0.0  , g: 0.0  , b: 0.0  , w: 0.3  },
        /*rose*/       LinRgbw {r: 0.5  , g: 0.0  , b: 0.0  , w: 0.5  },
        /*creme*/      LinRgbw {r: 0.5  , g: 0.5  , b: 0.0  , w: 0.5  },
        /*darkyellow*/ LinRgbw {r: 0.5  , g: 0.5  , b: 0.0  , w: 0.0  },
        /*random1*/    LinRgbw {r: 0.32 , g: 0.4  , b: 0.0  , w: 0.1  },
        /*random2*/    LinRgbw {r: 0.48 , g: 0.1  , b: 0.0  , w: 0.1  },
        /*random3*/    LinRgbw {r: 0.47 , g: 0.3  , b: 0.0  , w: 0.5  },
        /*random4*/    LinRgbw {r: 0.0  , g: 0.4  , b: 0.55 , w: 0.0  },
        /*random5*/    LinRgbw {r: 0.0  , g: 0.05 , b: 0.0  , w: 0.1  },
        /*random6*/    LinRgbw {r: 0.2  , g: 0.24 , b: 0.0  , w: 0.2  },
    ];
    #[rustfmt::skip]
    const REFERENCE_COLORS_HSV: [Hsv; COL_COUNT] = [
        /*red:*/       Hsv {h: 0.0  , s: 1.0  , v: 1.0  },
        /*green:*/     Hsv {h: 0.333, s: 1.0  , v: 1.0  },
        /*blue:*/      Hsv {h: 0.667, s: 1.0  , v: 1.0  },
        /*cyan*/       Hsv {h: 0.5  , s: 1.0  , v: 1.0  },
        /*mangenta:*/  Hsv {h: 0.833, s: 1.0  , v: 1.0  },
        /*yellow*/     Hsv {h: 0.167, s: 1.0  , v: 1.0  },
        /*black*/      Hsv {h: 0.0  , s: 0.0  , v: 0.0  },
        /*white*/      Hsv {h: 0.0  , s: 0.0  , v: 1.0  },
        /*lightgrey*/  Hsv {h: 0.0  , s: 0.0  , v: 0.7  },
        /*grey*/       Hsv {h: 0.0  , s: 0.0  , v: 0.5  },
        /*darkgrey*/   Hsv {h: 0.0  , s: 0.0  , v: 0.3  },
        /*rose*/       Hsv {h: 0.0  , s: 0.5  , v: 1.0  },
        /*creme*/      Hsv {h: 0.167, s: 0.5  , v: 1.0  },
        /*darkyellow*/ Hsv {h: 0.167, s: 1.0  , v: 0.5  },
        /*random1*/    Hsv {h: 0.2  , s: 0.8  , v: 0.5  },
        /*random2*/    Hsv {h: 0.035, s: 0.828, v: 0.58 },
        /*random3*/    Hsv {h: 0.106, s: 0.485, v: 0.97 },
        /*random4*/    Hsv {h: 0.533, s: 1.0  , v: 0.55 },
        /*random5*/    Hsv {h: 0.333, s: 0.333, v: 0.15 },
        /*random6*/    Hsv {h: 0.194, s: 0.545, v: 0.44 },
    ];
    #[rustfmt::skip]
    const REFERENCE_COLORS_SRGB: [Srgb; 14] = [
        /*red:*/       Srgb {r: 255, g: 0  , b: 0  , gamma: 2.2  },
        /*green:*/     Srgb {r: 0  , g: 255, b: 0  , gamma: 2.2  },
        /*blue:*/      Srgb {r: 0  , g: 0  , b: 255, gamma: 2.2  },
        /*cyan*/       Srgb {r: 0  , g: 255, b: 255, gamma: 2.2  },
        /*mangenta:*/  Srgb {r: 255, g: 0  , b: 255, gamma: 2.2  },
        /*yellow*/     Srgb {r: 255, g: 255, b: 0  , gamma: 2.2  },
        /*black*/      Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
        /*white*/      Srgb {r: 255, g: 255, b: 255, gamma: 2.2  },
        /*lightgrey*/  Srgb {r: 116, g: 116, b: 116, gamma: 2.2  },
        /*grey*/       Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
        /*darkgrey*/   Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
        /*rose*/       Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
        /*creme*/      Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
        /*darkyellow*/ Srgb {r: 0  , g: 0  , b: 0  , gamma: 2.2  },
    ];

    #[test]
    fn hsv_to_linrgb_conversion() {
        for i in 0..COL_COUNT {
            let col_converted: LinRgb = REFERENCE_COLORS_HSV[i].clone().into();

            assert_eq!(
                col_converted, REFERENCE_COLORS_LINRGB[i],
                "\n{:?} was converted to {:?} expected: {:?}",
                REFERENCE_COLORS_HSV[i], col_converted, REFERENCE_COLORS_LINRGB[i]
            );
        }
    }
    #[test]
    fn linrgb_to_hsv_conversion() {
        for i in 0..COL_COUNT {
            let col_converted: Hsv = REFERENCE_COLORS_LINRGB[i].clone().into();

            assert_eq!(
                col_converted, REFERENCE_COLORS_HSV[i],
                "\n{:?} was converted to {:?} expected: {:?}",
                REFERENCE_COLORS_LINRGB[i], col_converted, REFERENCE_COLORS_HSV[i]
            );
        }
    }
    #[test]
    fn linrgb_to_rgbw_conversion() {
        for i in 0..COL_COUNT {
            let col_converted: LinRgbw = REFERENCE_COLORS_LINRGB[i].clone().into();

            assert_eq!(
                col_converted, REFERENCE_COLORS_LINRGBW[i],
                "\n{:?} was converted to {:?} expected: {:?}",
                REFERENCE_COLORS_LINRGB[i], col_converted, REFERENCE_COLORS_LINRGBW[i]
            );
        }
    }
    #[test]
    fn linrgbw_to_linrgb_conversion() {
        for i in 0..COL_COUNT {
            let col_converted: LinRgb = REFERENCE_COLORS_LINRGBW[i].clone().into();

            assert_eq!(
                col_converted, REFERENCE_COLORS_LINRGB[i],
                "\n{:?} was converted to {:?} expected: {:?}",
                REFERENCE_COLORS_LINRGBW[i], col_converted, REFERENCE_COLORS_LINRGB[i]
            );
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use crate::color::*;
//     #[test]
//     fn linrgbToEncRgb(){
//         let rgbred:LinRgb = LinRgb {r:1.0, g: 0.0, b: 0.0};
//         let hsvred:LinRgb = Hsv {h:1.0, s: 1.0, v: 1.0}.into();
//
//
//         assert!(hsvred.r == rgbred.r && rgbred.g == hsvred.g && rgbred.b == hsvred.b);
//         assert_eq!(hsvred, rgbred)
//
//     }
// }
